def calculate_flames(name_1, name_2):
    """
    This function calculates the outcome of the Flames game based on two names.

    Args:
        name_1: The first name.
        name_2: The second name.

    Returns:
        A string representing the outcome (Friends, Lovers, Affectionate, Marriage, Enemy, or Siblings).
    """

    # Combine names and remove duplicates (case-insensitive)
    combined_name = "".join(set(name_1.lower() + name_2.lower()))

    # Count the remaining characters after removing duplicates
    count = len(combined_name)

    # Flames list
    flames = ["Friends", "Lovers", "Affectionate", "Marriage", "Enemy", "Siblings"]

    # Loop to eliminate elements from flames list
    while len(flames) > 1:
        eliminate = count % len(flames)
        flames = flames[eliminate:] + flames[:eliminate]  # Circular shift
        del flames[-1]  # Remove the last element

    # Return the remaining element (the outcome)
    return flames[0]


# Example usage
name_1 = input("Enter your name: ")
name_2 = input("Enter your partner's name: ")

outcome = calculate_flames(name_1, name_2)
print(f"According to the Flames game, you and {name_2} are {outcome}.")
