from rich import console
import time

console = console.Console()

# Ascii Arts
header = '''
[red]███████╗██╗         █████╗    ███╗   ███╗   ███████╗   ███████╗
[yellow]██╔════╝██║        ██╔══██╗   ████╗ ████║   ██╔════╝   ██╔════╝
[pink]█████╗  ██║        ███████║   ██╔████╔██║   █████╗     ███████╗
[magenta]██╔══╝  ██║        ██╔══██║   ██║╚██╔╝██║   ██╔══╝     ╚════██║
[cyan]██║██╗  ███████╗██╗██║  ██║██╗██║ ╚═╝ ██║██╗███████╗██╗███████║
[green]╚═╝╚═╝  ╚══════╝╚═╝╚═╝  ╚═╝╚═╝╚═╝     ╚═╝╚═╝╚══════╝╚═╝╚══════╝
'''
result = {
    'F': '''
         [red] _____     _                _     
         [red]|  ___| __(_) ___ _ __   __| |___ 
         [yellow]| |_ | '__| |/ _ \ '_ \ / _` / __|
         [blue]|  _|| |  | |  __/ | | | (_| \__ \\
         [green]|_|  |_|  |_|\___|_| |_|\__,_|___/
         ''',
    'L': '''
         [red] _                            
         [red]| |    _____   _____ _ __ ___ 
         [yellow]| |   / _ \ \ / / _ \ '__/ __|
         [blue]| |__| (_) \ V /  __/ |  \__ \\
         [green]|_____\___/ \_/ \___|_|  |___/
         ''',
    'A': '''
        [red]     _     __  __           _   _                   _       
        [red]    / \   / _|/ _| ___  ___| |_(_) ___  _ __   __ _| |_ ___ 
        [yellow]   / _ \ | |_| |_ / _ \/ __| __| |/ _ \| '_ \ / _` | __/ _ \\
        [blue]  / ___ \|  _|  _|  __/ (__| |_| | (_) | | | | (_| | ||  __/
        [green] /_/   \_\_| |_|  \___|\___|\__|_|\___/|_| |_|\__,_|\__\___|
         ''',
    'M': '''
         [red] __  __                 _                  
         [red]|  \/  | __ _ _ __ _ __(_) __ _  __ _  ___ 
         [yellow]| |\/| |/ _` | '__| '__| |/ _` |/ _` |/ _ \\
         [blue]| |  | | (_| | |  | |  | | (_| | (_| |  __/
         [green]|_|  |_|\__,_|_|  |_|  |_|\__,_|\__, |\___|
         [green]                                |___/      
         ''',
    'E': '''
         [red] _____                      _           
         [red]| ____|_ __   ___ _ __ ___ (_) ___  ___ 
         [yellow]|  _| | '_ \ / _ \ '_ ` _ \| |/ _ \/ __|
         [blue]| |___| | | |  __/ | | | | | |  __/\__ \\
         [green]|_____|_| |_|\___|_| |_| |_|_|\___||___/
         ''',
    'S': '''
         [red] ____  _ _     _ _                 
         [red]/ ___|(_) |__ | (_)_ __   __ _ ___ 
         [yellow]\___ \| | '_ \| | | '_ \ / _` / __|
         [blue] ___) | | |_) | | | | | | (_| \__ \\
         [green]|____/|_|_.__/|_|_|_| |_|\__, |___/
         [green]                        |___/     
         '''
    }

delay = 1.5

console.rule('[red]*[yellow]*[blue]* [green]WELCOME TO [blue]*[yellow]*[red]*')
console.print(header, justify='center')
console.rule()

time.sleep(delay)
padding = ' ' * 8
name_1 = console.input(padding + "Enter [green]your name: ").lower()
name_2 = console.input(padding + "Enter your [magenta]partner name: ").lower()

print()
console.rule('Calculating [red]F-[yellow]L-[pink]A-[magenta]M-[cyan]E-[green]S')
# Removing similar letters among two names
for i in name_1:
    if i in name_2:
        name_2 = name_2.replace(i, '', 1)
        name_1 = name_1.replace(i, '', 1)


length = len(name_1 + name_2)
flames = ["F", "L", "A", "M", "E", "S"]
index = 0
while len(flames) > 1:
    index = length % len(flames) - 1
    if index >= 0:
        # We are separating the list into two halves
        # excluding the element that need to be removed.
        # Then we are flipping the sides so that the next
        # removal got right.
        right = flames[index + 1:]
        left = flames[: index]
        time.sleep(delay)
        console.print(padding + '[red]Striked out [blue]' + flames[index])
        flames = right + left
    else:
        # If we need to remove the last element
        console.print(padding + '[red]Striked out [blue]' + flames[-1])
        del flames[-1]
time.sleep(delay)
print()
console.rule('[red]*[yellow]*[blue]* [green]Result [blue]*[yellow]*[red]*')

time.sleep(delay)
console.print(result[flames[0]])
